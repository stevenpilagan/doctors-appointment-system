<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'mobile_number', 'phone_number', 'address', 'birthdate', 'gender', 'nationality', '', 'password', 'user_type', 'job_title',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function register( $data ) {
        if( $data ) {
            $registered = self::create($data);

            if( $registered ) {
                return [
                    'success' => true,
                    'message' => 'New user created succesfully.'
                ];
            }

            return [
                'error' => true,
                'message' => 'Error occured while creating new user.'
            ];
        }
    }

    public static function getUserByEmail( $email ) {
        if( $email ) {
            return self::where( 'email', $email )->first();
        }
    }

    public static function getUserById( $id ) {
        if( $id ) {
            return self::where( 'id', $id )->first();
        }
    }
}
