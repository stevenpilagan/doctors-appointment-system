<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterForm;
use App\Http\Requests\LoginForm;
use App\User;

class UserController extends Controller
{
	/**
	 * [register description]
	 * @param  RegisterForm $request [description]
	 * @return [type]                [description]
	 */
    public function register(RegisterForm $request) {

		//Hash password from request
    	$request->merge([
    		'password' => Hash::make( $request->password )
    	]);

    	//save data to users table
    	$user = User::register( $request->all() );

    	//return the response
	    return response()->json( $user );
    }

    /**
     * [login description]
     * @return [type] [description]
     */
    public function login(LoginForm $request) {
    	$isUser = User::getUserByEmail( $request->email );
    	$passwordMatched = Hash::check( $request->password, $isUser->password );

    	if( $isUser && $passwordMatched ) {
    		return response()->json([
	    		'success' => true,
	    		'message' => 'User is valid.'
	    	]);
    	}

    	return response()->json([
    		'error' => true,
    		'message' => 'Email or password is invalid.'
    	]);
    }

    /**
     * [logout description]
     * @return [type] [description]
     */
    public function logout() {
    	return 'logout';
    }
}
