//pages
Vue.component('MainContainer', require('./pages/MainContainerComponent.vue'));
Vue.component('LoginPage', require('./pages/LoginPageComponent.vue'));
Vue.component('RegisterPage', require('./pages/RegisterPageComponent.vue'));
Vue.component('UserPage', require('./pages/UserPageComponent.vue'));

//sections
Vue.component('LoginForm', require('./sections/forms/LoginFormComponent.vue'));
Vue.component('RegisterForm', require('./sections/forms/RegisterFormComponent.vue'));