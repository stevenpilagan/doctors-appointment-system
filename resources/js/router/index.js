import Vue from 'vue'
import VueRouter from 'vue-router'
import Register from '../components/pages/RegisterPageComponent'
import Login from '../components/pages/LoginPageComponent'
import User from '../components/pages/UserPageComponent'
import Admin from '../components/pages/AdminPageComponent'

Vue.use(VueRouter)

export default new VueRouter({
	mode: 'history',
  	routes: [
	  	{
			path: '/',
			name: '/',
			component: Register
	    },
	    {
			path: '/login',
			name: 'Login',
			component: Login
	    },
	    {
			path: '/register',
			name: 'Register',
			component: Register
	    },
	    {
			path: '/user',
			name: 'User',
			component: User
	    },
	    {
			path: '/admin',
			name: 'Admin',
			component: Admin
	    }
  	]
})
